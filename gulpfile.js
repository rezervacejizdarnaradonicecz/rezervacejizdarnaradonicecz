// --- PLUGINS ---
var gulp = require('gulp');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var svgSymbols = require('gulp-svg-symbols');
var image = require('gulp-image');
var nunjucks = require('gulp-nunjucks');
var htmlbeautify = require('gulp-html-beautify');
var livereload = require('gulp-livereload');
var fs = require('fs');




// --- CLEAN ---
gulp.task('clean', function() {
	gulp.src('dist')
		.pipe(clean());
});



// --- COPY ---
gulp.task('copy', function(){
	gulp.src(['node_modules/popper.js/dist/**/*'])
		.pipe(gulp.dest('dist/popper.js/'));
	gulp.src(['node_modules/jquery/dist/**/*'])
		.pipe(gulp.dest('dist/jquery/'));
});



// --- CSS ---
gulp.task('css', function() {
	gulp.src('src/scss/main.scss')
		//.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(cleanCSS({
			rebase: false,
			level: 2
		}))
		.pipe(concat('style.min.css'))
		//.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('dist/css/'));
});



// --- JS ---
var sourceJS = require('./src/js/main.json');
gulp.task('js', function() {
	gulp.src(sourceJS)
		//.pipe(sourcemaps.init())
		.pipe(concat('scripts.min.js'))
		.pipe(uglify())
		//.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('dist/js/'));
});



// --- IMAGES ---
gulp.task('images', function() {
	gulp.src('src/img/**/*')
		.pipe(image())
		.pipe(gulp.dest('dist/img'));
});



// --- FAVICON ---
gulp.task('favicon', function() {
	gulp.src('src/favicon/**/*')
		.pipe(image())
		.pipe(gulp.dest('dist/favicon'));
});



// --- SPRITES ---
gulp.task('sprites', function () {
	gulp.src('src/svg/icons/*.svg')
		.pipe(svgSymbols({
			id: 'icon-%f',
			templates: [
				'default-svg',
				'default-css',
				'default-demo'
			],
			transformData: function(svg, defaultData, options) {
				width = (1/svg.height)*svg.width;
				return {
					id: defaultData.id,
					class: '.'+defaultData.id,
					width: width + 'em',
					height: '1em'
				};
			}
		}))
		.pipe(gulp.dest('dist/svg/icons/'))
		.pipe(gulp.dest('src/templates/svg/icons/'))
});




// --- HTML ---
gulp.task('templates', function() {
	gulp.src('src/templates/**.html')
	.pipe(nunjucks.compile({
		currentTime: new Date().getTime()
	}))
	.pipe(htmlbeautify({
		'indent_with_tabs': true,
		'max_preserve_newlines': 1,
	}))
	.pipe(gulp.dest('dist/templates'));
});


// --- DEFAULT ---
gulp.task('default', function(){
	gulp.start('copy', 'css', 'js', 'favicon', 'images', 'sprites', 'templates');
});



// --- WATCH ---
gulp.task('watch',function(){
	gulp.watch(['src/scss/**/*.scss'],['css']);
	gulp.watch(['src/js/**/*.js'],['js']);
	gulp.watch(['src/templates/**/*.html'],['templates']);
	
	// LIVERELOAD CHROME EXTENTION
	livereload.listen();
	gulp.watch(['dist/css/*.css']).on('change',livereload.changed);
	gulp.watch(['dist/js/*.js']).on('change',livereload.changed);
	gulp.watch(['dist/templates/*.html']).on('change',livereload.changed);
});