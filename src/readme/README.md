# {{ package.name }}
{{ package.description }}
{% if package.version %} **Version**: {{ package.version }}{% endif -%}
{% if package.license %} | **License**: {{ package.license }}{% endif %}

## Installation
Run `$ npm install` in root direcory.

{% if package.author -%}
## Author
{{ package.author.name -}}
{% if package.author.email -%}
, [{{ package.author.email }}](mailto:{{ package.author.email }})
{% endif -%}
{% if package.author.url -%}
[{{ package.author.url }}]({{ package.author.url }})
{% endif -%}
{% endif -%}

{% if package.contributors|length -%}
## Contributors
{% for contributor in package.contributors -%}
{{ contributor.name -}}
{% if contributor.email -%}
, [{{ contributor.email }}](mailto:{{ contributor.email }})
{% endif -%}
{% if contributor.url -%}
[{{ contributor.url }}]({{ contributor.url }})
{% endif -%}
{% endfor -%}
{% endif -%}

{% if package.scripts|length -%}
## Scripts
{% for key, value in package.scripts -%}
{{key}}:  `$ {{value}}`
{% endfor -%}
{% endif -%}

{% if devDependencies|length -%}
## Dev Dependencies
| Name | Description |
| --- | --- |
{% for item in devDependencies -%}
| [{{item.name}}](https://ghub.io/{{item.name}}) |  {{item.description}} |
{% endfor -%}
{% endif -%}

{% if dependencies|length -%}
## Dependencies
| Name | Description |
| --- | ---
{% for item in dependencies -%}
| [{{item.name}}](https://ghub.io/{{item.name}}) |  {{item.description}} |
{% endfor -%}
{% endif -%}